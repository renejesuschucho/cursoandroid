package com.example.ventasdomicilioipiales;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class NewSaleActivity extends AppCompatActivity {
    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAddress;
    private TextInputEditText etAddress;
    private TextInputLayout tiAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etNumber;
    private MaterialAutoCompleteTextView etPeriodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;
    private TextInputEditText etDate;
    private Date selectedDate;
    private AppCompatButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);
        initUI();
    }

    private void initUI() {
        tilDate = findViewById(R.id.til_date);
        etDate = findViewById(R.id.et_date);
        tilDate.setEndIconOnClickListener(v -> onDateClick());
    }

    private void onDateClick() {
        long today =MaterialDatePicker.todayInUtcMilliseconds();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(today);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        CalendarConstraints constraint = new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.from(calendar.getTimeInMillis())).build();

        MaterialDatePicker<Long> datePicker= MaterialDatePicker.Builder.datePicker()
             .setTitleText("Seleccione la fecha").setSelection(calendar.getTimeInMillis())
                .setCalendarConstraints(constraint).build();
        datePicker.addOnPositiveButtonClickListener(this::setSelectedDate);
        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void setSelectedDate(Long selection) {
        selectedDate = new Date(selection);
        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }
}