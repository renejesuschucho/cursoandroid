package com.example.ventasdomicilioipiales;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

public class PaymentsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private RecyclerView rvPayments;
    private FloatingActionButton bntNewSale;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        iniUI();
    }

    private void iniUI() {
        drawerLayout = findViewById(R.id.drawer_layout);
        MaterialToolbar toolbar = findViewById(R.id.app_toolbar);
        toolbar.setNavigationOnClickListener(v ->drawerLayout.open());
        navigationView = findViewById(R.id.nv_payments);
        navigationView.setNavigationItemSelectedListener(this::onMenuItemClick);
        rvPayments = findViewById(R.id.rv_payments);
        bntNewSale = findViewById(R.id.btn_new_sale);

        //Programming button floating
        bntNewSale.setOnClickListener(v -> onNewSaleClick());
    }

    //Method of button floating
    private void onNewSaleClick() {
        startActivity(new Intent(PaymentsActivity.this, NewSaleActivity.class));

    }

    //Method for open options on Menu
    private boolean onMenuItemClick(MenuItem menuItem) {
        menuItem.setChecked(true);
        drawerLayout.close();
        if (menuItem.getItemId() == R.id.ni_sales) { //Ventas
            startActivity(new Intent(PaymentsActivity.this, NewSaleActivity.class));
        }
        return true;
    }




}